Source: cloud-init
Maintainer: Debian Cloud Team <debian-cloud@lists.debian.org>
Uploaders:
 Bastian Blank <waldi@debian.org>,
 Charles Plessy <plessy@debian.org>,
 Thomas Goirand <zigo@debian.org>,
Section: admin
Priority: optional
Build-Depends:
 debhelper (>= 10),
 dh-python,
 dh-systemd,
 iproute2,
 pep8,
 po-debconf,
 pylint,
 python3-all,
 python3-setuptools,
Build-Depends-Indep:
 python-pyflakes,
 python3-configobj,
 python3-contextlib2,
 python3-httpretty (>= 0.9.5),
 python3-jinja2,
 python3-jsonpatch,
 python3-jsonschema,
 python3-mock,
 python3-nose,
 python3-oauthlib,
 python3-pep8,
 python3-prettytable,
 python3-requests,
 python3-serial,
 python3-six,
 python3-unittest2,
 python3-yaml,
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/git/cloud/cloud-init.git
Vcs-Git: https://anonscm.debian.org/git/cloud/cloud-init.git
Homepage: https://launchpad.net/cloud-init

Package: cloud-init
Architecture: all
Depends:
 cloud-guest-utils,
 fdisk | util-linux (<< 2.29.2-3~),
 gdisk,
 ifupdown,
 locales,
 lsb-base,
 lsb-release,
 net-tools,
 procps,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 eatmydata,
 sudo,
Suggests:
 btrfs-progs,
 e2fsprogs,
 xfsprogs,
Description: initialization system for infrastructure cloud instances
 Cloud-init provides a framework and tool to configure and customize virtual
 machine instances for Infrastructure-as-a-Service (IaaS) clouds platforms. It
 can for example set a default locale and hostname, generate SSH private host
 keys, install SSH public keys for logging into a default account, set up
 ephemeral mount points, and run user-provided scripts.
 .
 Various methods are supported for passing data to the instance at launch
 time, including the standard interfaces of multiple platforms.
